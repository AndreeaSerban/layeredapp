using BusinessLayer;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Tests
{
    [TestFixture]
    public class FacultyManagerTests
    {
        Mock<IFacultyManagerDAL> _facultyManagerDALMock;
        private IFacultyManager _facultyManager;
        private const int LOWER_THAN_ACCEPTED_ID = -1;
        private const int ACCEPTED_ID = 5;

        [SetUp]
        public void Setup()
        {
            _facultyManagerDALMock = new Mock<IFacultyManagerDAL>();
        }

        [Test]
        public void FacultyManagerCtorNullIFacultyManagerDAL()
        {
            ArgumentNullException ex = Assert.Throws<ArgumentNullException>(() => new FacultyManager(null));
            Assert.That(It.Equals("facultyManagerDAL", ex.ParamName));
        }

        [Test]
        public void DeleteIdLowerThanAcceptedValueTest()
        {
            _facultyManager = new FacultyManager(_facultyManagerDALMock.Object);
            Assert.False(_facultyManager.Delete(LOWER_THAN_ACCEPTED_ID));
        }

        [Test]
        public void DeleteIdAcceptedValueTest()
        {
            _facultyManager = new FacultyManager(_facultyManagerDALMock.Object);
            _facultyManagerDALMock.Setup(m => m.DeletePerson(It.IsAny<int>())).Returns(true).Verifiable();

            Assert.True(_facultyManager.Delete(ACCEPTED_ID));
        }

        [Test]
        public void GetPersonByIdLowerThanAcceptedValueTest()
        {
            _facultyManager = new FacultyManager(_facultyManagerDALMock.Object);
            Assert.AreSame(null, _facultyManager.GetPersonById(LOWER_THAN_ACCEPTED_ID));
        }

        [Test]
        public void GetPersonByIdAcceptedValueTest()
        {
            _facultyManager = new FacultyManager(_facultyManagerDALMock.Object);
            _facultyManagerDALMock.Setup(m => m.GetPersonBLFromFileById(It.IsAny<int>())).Returns(new PersonBL()).Verifiable();

            Assert.IsTrue(_facultyManager.GetPersonById(ACCEPTED_ID) is PersonBL);
        }

        [Test]
        public void InsertPeopleNullPersonTest()
        {
            _facultyManager = new FacultyManager(_facultyManagerDALMock.Object);
            ArgumentNullException ex = Assert.Throws<ArgumentNullException>(() => _facultyManager.InsertPeople(null));
            Assert.That(It.Equals("person", ex.ParamName));
        }

        [Test]
        public void InsertPeopleNotANullPersonTest()
        {
            _facultyManager = new FacultyManager(_facultyManagerDALMock.Object);
            _facultyManagerDALMock.Setup(m => m.PutPersonInFile(It.IsAny<PersonInsertBL>())).Returns(true).Verifiable();

            Assert.IsTrue(_facultyManager.InsertPeople(new PersonInsertBL()));
        }

        [Test]
        public void UpdatePeopleIdLowerThanAcceptedValueTest()
        {
            _facultyManager = new FacultyManager(_facultyManagerDALMock.Object);
            Assert.IsFalse(_facultyManager.Update(LOWER_THAN_ACCEPTED_ID, new PersonInsertBL()));
        }

        [Test]
        public void UpdatePeopleIdAcceptedValueTest()
        {
            _facultyManager = new FacultyManager(_facultyManagerDALMock.Object);
            _facultyManagerDALMock.Setup(m => m.UpdatePerson(It.IsAny<int>(), It.IsAny<PersonInsertBL>())).Returns(true).Verifiable();
            Assert.IsTrue(_facultyManager.Update(ACCEPTED_ID, new PersonInsertBL()));
        }

        [Test]
        public void ListPeopleListTest()
        {
            _facultyManager = new FacultyManager(_facultyManagerDALMock.Object);
            _facultyManagerDALMock.Setup(m => m.GetListFromFile()).Returns(new List<PersonBL>()).Verifiable();
            Assert.IsTrue(_facultyManager.ListPeople() is List<PersonBL>);
        }

        [Test]
        public void ListStudentsListTest()
        {
            _facultyManager = new FacultyManager(_facultyManagerDALMock.Object);
            _facultyManagerDALMock.Setup(m => m.GetStudentListFromFile()).Returns(new List<StudentBL>()).Verifiable();
            Assert.IsTrue(_facultyManager.ListStudents() is List<StudentBL>);
        }

        [Test]
        public void ListTeachersListTest()
        {
            _facultyManager = new FacultyManager(_facultyManagerDALMock.Object);
            _facultyManagerDALMock.Setup(m => m.GetProfListFromFile()).Returns(new List<ProfBL>()).Verifiable();
            Assert.IsTrue(_facultyManager.ListProfs() is List<ProfBL>);
        }
    }
}