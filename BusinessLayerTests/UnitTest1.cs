using BusinessLayer;
using Moq;
using NUnit.Framework;
using System;

namespace Tests
{
    [TestFixture]
    public class Tests
    {
        Mock<IFacultyManagerDAL> _facultyManagerDALMock;
        private IFacultyManager _facultyManager;
        private Mock<ILogger> _loggerMock;
        private const int LOWER_THAN_ACCEPTED_ID = -1;
        private const int ACCEPTED_ID = 5;

        [SetUp]
        public void Setup()
        {
            _facultyManagerDALMock = new Mock<IFacultyManagerDAL>();
            _loggerMock = new Mock<ILogger>();
        }

        [Test]
        public void FacultyManagerCtorNullIFacultyManagerDAL()
        {
            ArgumentNullException ex = Assert.Throws<ArgumentNullException>(() => new FacultyManager(null, _loggerMock.Object));
            Assert.That(It.Equals("facultyManagerDAL", ex.ParamName));
        }

        [Test]
        public void FacultyManagerCtorNullILogger()
        {
            ArgumentNullException ex = Assert.Throws<ArgumentNullException>(() => new FacultyManager(_facultyManagerDALMock.Object, null));
            Assert.That(It.Equals("logger", ex.ParamName));
        }

        [Test]
        public void DeleteIdLowerThanAcceptedValueTest()
        {
            _facultyManager = new FacultyManager(_facultyManagerDALMock.Object, _loggerMock.Object);
            Assert.False(_facultyManager.Delete(LOWER_THAN_ACCEPTED_ID));
        }

        [Test]
        public void DeleteIdAcceptedValueTest()
        {
            _facultyManager = new FacultyManager(_facultyManagerDALMock.Object, _loggerMock.Object);
            _facultyManagerDALMock.Setup(m => m.DeletePerson(It.IsAny<int>())).Returns(true).Verifiable();

            Assert.True(_facultyManager.Delete(ACCEPTED_ID));
        }
        
    }
}