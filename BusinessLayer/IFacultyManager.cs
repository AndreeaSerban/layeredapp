﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer
{
    public interface IFacultyManager
    {
        List<PersonBL> ListPeople();
        List<StudentBL> ListStudents();
        List<ProfBL> ListProfs();
        bool InsertPeople(PersonInsertBL person);
        PersonBL GetPersonById(int id);
        bool Update(int id, string firstName, string lastName, DateTime birthDay);
        bool Delete(int id);
    }
}
