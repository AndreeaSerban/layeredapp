﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer
{
    /// <summary>
    /// 
    /// </summary>
    public interface IFacultyManagerDAL
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        string[] ReadAllFileLines();
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<PersonBL> GetListFromFile();

        /// <summary>
        ///     Gets a person by id 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        PersonBL GetPersonBLFromFileById(int id);

        //PersonDAL GetPersonFromFileById(int id);
        List<StudentBL> GetStudentListFromFile();
        List<ProfBL> GetProfListFromFile();
        bool PutPersonInFile(PersonInsertBL personDAL);
        bool UpdatePerson(int id, PersonInsertBL personDAL);
        bool DeletePerson(int id);

    }
}
