﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace BusinessLayer
{
    public class FacultyManager : IFacultyManager
    {

        private IFacultyManagerDAL _facultyManagerDAL;
        private ILogger _logger;

        // ReSharper disable once InconsistentNaming
        public FacultyManager(IFacultyManagerDAL facultyManagerDAL, ILogger logger)
        {
            _facultyManagerDAL = facultyManagerDAL ?? throw new ArgumentNullException(nameof(facultyManagerDAL));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public bool Delete(int id)
        {
            _logger.Information("started delete for person with id");
            return _facultyManagerDAL.DeletePerson(id);
        }

        public PersonBL GetPersonById(int id)
        {
            if(id >= 0)
            {
                return _facultyManagerDAL.GetPersonBLFromFileById(id);
            }
            return null;
        }

        public bool InsertPeople(PersonInsertBL person)
        { 
            return _facultyManagerDAL.PutPersonInFile(person);
        }

        public List<PersonBL> ListPeople()
        {
            return _facultyManagerDAL.GetListFromFile();
        }

        public List<StudentBL> ListStudents()
        {  
            return _facultyManagerDAL.GetStudentListFromFile();
        }

        public List<ProfBL> ListProfs()
        {
            return _facultyManagerDAL.GetProfListFromFile();
        }

        public bool Update(int id, string firstName, string lastName, DateTime birthDay)
        {
            PersonInsertBL personInsertBL = new PersonInsertBL(id, firstName, lastName, birthDay);
            return _facultyManagerDAL.UpdatePerson(id, personInsertBL);
        }
    }
}
