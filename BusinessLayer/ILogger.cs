﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer
{
    public interface ILogger
    {
        void Information(string message);
    }
}
