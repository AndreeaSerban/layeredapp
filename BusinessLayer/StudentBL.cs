﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer
{
    public class StudentBL : PersonBL
    {
        public int Year { get; set; }
        public int Group { get; set; }

        public StudentBL() : base()
        {

        }

        public StudentBL(int year, int group, int id, string firstName, string lastName, int age):base(id:id,firstName:firstName,lastName:lastName,age:age)
        {
            Year = year;
            Group = group;
        }

        public override string ToString()
        {
            return base.ToString() + " Year: " + Year + " Group: " + Group;
        }
    }
}
