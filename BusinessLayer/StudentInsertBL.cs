﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class StudentInsertBL : PersonInsertBL
    {
        public int Year { get; set; }
        public int Group { get; set; }

        public StudentInsertBL() : base()
        {

        }

        public StudentInsertBL(int year, int group, int id, string firstName, string lastName, DateTime birthDate) : base(id: id, firstName: firstName, lastName: lastName, birthDate: birthDate)
        {
            Year = year;
            Group = group;
        }

        public override string ToString()
        {
            return base.ToString() + " Year: " + Year + " Group: " + Group;
        }
    }
}
