﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class ProfInsertBL : PersonInsertBL
    {
        public double Salary { get; set; }

        public ProfInsertBL() : base()
        {

        }

        public ProfInsertBL(double salary, int id, string firstName, string lastName, DateTime birthdate) : base(id: id, firstName: firstName, lastName: lastName, birthDate: birthdate)
        {
            Salary = salary;
        }

        public override string ToString()
        {
            return base.ToString() + " Salary: " + Salary;
        }
    }
}
