﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer
{
    public class PersonBL
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public int Id { get; set; }

        public PersonBL()
        {

        }

        public PersonBL(int id, string firstName, string lastName, int age)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Age = age;
        }

        public override string ToString()
        {
            return FirstName + " " + LastName + " " + Age;
        }
    }
}
