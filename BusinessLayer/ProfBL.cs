﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer
{
    public class ProfBL : PersonBL
    {
        public double Salary { get; set; }

        public ProfBL() : base()
        {

        }

        public ProfBL(double salary, int id, string firstName, string lastName, int age):base(id:id,firstName:firstName,lastName:lastName,age:age)
        {
            Salary = salary;
        }

        public override string ToString()
        {
            return base.ToString() + " Salary: " + Salary;
        }
    }
}
